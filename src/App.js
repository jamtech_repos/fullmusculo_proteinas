import React, { Component } from 'react';
import Header from './component//header/header'
import Content from './component/content/content'
import Footer from './component/footer/footer'
import Menu from './component/menu/menu'
import Recomendados from './component/recomendados/recomendados'
import axios from 'axios';
import './App.css';
import TextContent from './component/textContent/text';
import Shared from './component/shared/shared';
import ReactGA from 'react-ga'
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this);
    ReactGA.initialize('UA-62287527-1')
  ReactGA.pageview('/suplementos-deportivos')
    axios.get('https://ipapi.co/json/').then(
      (response) => {
        console.log('IP API',response);
        localStorage.setItem('country',response.data.country_name)
      }
      ).catch(function (error) {
        console.log(error);
      })
      this.state = {
        menuselect: 1,
        prevScrollpos: window.pageYOffset,
        visible: false};
  }
  handleChange(i) {
    this.setState({
      menuselect: i
    });
  };

  handleScroll = () => {  
    const currentScrollPos = window.pageYOffset;
    const visible = currentScrollPos > 1000; 
    this.setState({
      prevScrollpos: currentScrollPos,
      visible
    });
  };
  
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }
  
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  render() {
    
    return (
      
      <div className="App">
        <Header />
        <div className="fondo">
          {/*<Recomendados menuselect={this.state.menuselect} /> */}
          <div>
            <Menu onMenuChange={this.handleChange}/>
            <Content menuselect={this.state.menuselect}/>
          </div>
          <Shared />
          <TextContent /> 
        </div>
        
        
        <Footer />
      </div>
    );
  }
}

export default App;
