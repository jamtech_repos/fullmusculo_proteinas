import React, {Component}from 'react'
import PropTypes from "prop-types";
import recomendados from '../../models/recomendados'
import { Button} from 'react-bootstrap'
import recomendado from '../../assets/img/recomendados.png'
import './recomendados.css'
class Recomendado extends Component {
    constructor(props) { 
        super(props)
        this.state = {
            link: localStorage.getItem('country') !== 'Spain' ?
                'https://www.amazon.com/gp/aws/cart/add.html':
                'https://www.amazon.es/gp/aws/cart/add.html',
            imgs: ['','',''],
            menu: 1
        }
        /* this.getImagesProducts() */
    }

    /* getImagesProducts(menu) {
        for (let i = 0; i < recomendados.length; i++) {
            if (localStorage.getItem('country') !== 'Spain') {
                axios.get('https://fullmusculo.com/api/aws?tag=fullmusculo0c-20&asin='+ recomendados[i].asin_u)
                .then((response) => {
                    let imgs = this.state.imgs
                     imgs[i] = response.data.data.SmallImage[0].URL[0]
                    this.setState({imgs: imgs })
                }).catch((err) => {
                    console.log(err)
                })
            } else {
                axios.get('https://fullmusculo.com/api/aws?tag=fullm-21&asin='+ recomendados[i].asin_es)
                .then((response) => {
                    let imgs = this.state.imgs[i]
                    imgs[i] = response.data.data.SmallImage[0].URL[0]
                    this.setState({imgs: imgs })
                }).catch((err) => {
                    console.log(err)
                })
            } 
            
        }
    } */
    render() {
        return(
            <div className="container">
                <div className="recos">
                    <h3>Combo Recomendado</h3>
                    <img className="reco-img" src={recomendado} alt=""></img>
                   {/*  <div className="row">
                        {
                            recomendados.map( (reco, i) =>
                                <div className="col-4">
                                    <OverlayTrigger
                                        key='top'
                                        placement='top'
                                        overlay={
                                            <Tooltip id={`tooltip-${reco.nombre}`}>
                                            {reco.nombre}
                                            </Tooltip>
                                        }
                                        >
                                        <div>
                                            <div className="reco-img">
                                                <img src={this.state.imgs[i]} alt={reco.nombre}></img>
                                            </div>
                                          
                                        </div>
                                    </OverlayTrigger>
                                </div>
                                
                            )
                        }
                    </div> */}
                </div>
                
                <div style={{display: 'flex', marginBottom: '20px'}}>
                    <Button 
                        onClick={()=> window.open(this.state.link + 
                            `?ASIN.1=${
                                localStorage.getItem('country') !== 'Spain' ?
                                recomendados[0].asin_u:
                                recomendados[0].asin_es
                            }&Quantity.1=1&ASIN.2=${
                                localStorage.getItem('country') !== 'Spain' ?
                                recomendados[1].asin_u:
                                recomendados[1].asin_es
                            }&Quantity.2=1&ASIN.3=${
                                localStorage.getItem('country') !== 'Spain' ?
                                recomendados[2].asin_u:
                                recomendados[2].asin_es
                            }&Quantity.3=1`, 
                            "_blank")} 
                        style={{margin: '0 auto'}}> 
                            Comprar al mejor precio
                    </Button>
                </div>
                
            </div>
        )
    }
}

Recomendado.propTypes = {
    menuselect: PropTypes.number
};
Recomendado.defaultProps = {
    menuselect: 1
};
export default Recomendado