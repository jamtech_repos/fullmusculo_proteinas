import React, {Component} from 'react';
import './text.css'
import ScrollUpButton from "react-scroll-up-button";
import {Button} from 'react-bootstrap'
import classnames from "classnames";


class TextContent extends Component { 
   constructor() {
        super()
        this.state = {
            showmore: true,
            visible: false,
            width: window.innerWidth
        }
        this.myRef = React.createRef()
    }
    scrollToMyRef = () => window.scrollTo(0, this.myRef.current.offsetTop)   
    handleScroll = () => {  
        const currentScrollPos = window.pageYOffset;
        const visible = (currentScrollPos > 1000) && (!this.state.showmore || this.state.width > 992); 
        this.setState({
          prevScrollpos: currentScrollPos,
          visible
        });
      };
      
      componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
      }
      
      componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
      }
    render() {
        return (
            <div className="container">
                {/* <div style={{display: 'flex',justifyContent: 'center'}}>
                    <Button onClick={() => this.scrollToMyRef()} >Cómo funciona nuestra guía de suplementos</Button>
                </div> */}
                
                <div className="container" style={{ paddingBottom: '100px'}}>
                <h1 ref={this.myRef} style={{textAlign: 'center',marginBottom: '40px'}}>Cómo funciona nuestra guía de suplementos?</h1>
                <p>La nutrición deportiva es un campo en constante evolución con cientos de estudios publicados cada años.</p>
                <p>Solo en el año 2017 se publicaron más de 2082 artículos relacionados al mundo de la nutrición deportiva.</p>
                <p>Por lo que mantenerse actualizado suele ser algo complicado.</p>
                <p>Evaluar la evidencia científica que hay detrás de cada producto o suplemento deportivo es una habilidad clave que todo profesional del deporte y la salud debería tener.</p>
                <p>Para mantener actualizado a los interesados la ISSN o International Society of Sports
                Nutrition por sus siglas en inglés ha realizado una revisión de sus recomendaciones y
                nosotros la hemos llevado a una gráfica para que sea de más fácil entendimiento e
                interacción.</p>
                <p>En el documento realizado por la ISSN se realiza un resumen de la eficacia de los
                muchos suplementos que dicen promover la hipertrofia muscular y mejorar el
                rendimiento físico.</p>
                <p>Que es precisamente tema de nuestro interés y donde hemos concentrado toda
                nuestra atención para diseñar la gráfica interactiva que puedes ver arriba del texto.</p>
                <p>Basándose en la evidencia científica disponible de la eficacia y seguridad de los
                suplementos deportivos en discusión se han ubicado en una de las tres categorías
                descritas a continuación tomando en cuenta la calidad y la cantidad del soporte
                científico disponible a día de hoy:</p>
                <p>Las tres categorias son:</p>
                <ul>
                    <li><p>Fuerte evidencia que respalde la eficacia y seguridad.</p></li>
                    <li><p>Evidencia limitada o mixta que respalde la eficacia.</p></li>
                    <li><p>Poca o no evidencia que respalde la eficacia y/o seguridad del suplemento.</p></li>
                </ul>
                <p>Cómo la ultima versión de el documento publicado por la ISSN fue en 2010, la
                categorización no ha cambiado pero si muchos suplementos que han aparecido en el
                mercado y han sido revisados se han incluido en las categorías a la que correspondan.</p>
                <p>Y otros que ya estaban ubicados en alguna categoría se han movido o se han
                mantenido si alguna revisión más reciente da más o menos respaldo con más o menos
                evidencia con respecto a su efectividad y seguridad.</p>
                <p>Y como es probable que se hagan más revisiones y sigan apareciendo artículos con
                nuevos estudios de los suplementos deportivos en mención esta gráfica y este
                documento estarán en constante cambio y adaptación para mantenernos siempre actualizados.
                </p>
                {(!this.state.showmore || this.state.width > 992) ?
                <>
                    <h2 style={{margin: '40px 0'}}>Qué es una ayuda ergogénica?</h2>
                    <p>Antes de meternos en terreno de los suplementos deportivos tenemos que entender lo
                    que es una ayuda ergogénica.</p>
                    <p>No es más que cualquier técnica de entrenamiento, dispositivo mecánico, ingrediente,
                    práctica nutricional, método farmacológico o técnica psicológica que tenga la capacidad
                    de mejorar el rendimiento deportivo y/o mejorar las adaptaciones.</p>
                    <p>Las ayudas ergogénicas pueden ayudar a preparar a una persona previo a alguna
                    actividad física o ejercicio, a mejorar la eficiencia durante el ejercicio y/o a mejorar su
                    recuperación después del ejercicio o prevenir lesiones durante el entrenamiento.</p>
                    <p>Aún cuando la definición a primera vista parece sencilla y sería fácil de aplicar a los
                    suplementos hay mucho debate sobre el valor real ergogénico que pueden tener
                    algunos suplementos nutricionales.
                    </p>
                    <p>Hay un consenso que sugiere que un suplemento deportivo se puede definir como
                    ergogénico cuando estudios revisados demuestran que el suplemento es capaz de
                    mejorar significativamente el rendimiento del ejercicio luego de semanas a meses de su
                    primera toma.
                    </p>
                    <p>Un suplemento se consideraría ergogénico si puede promover aumento de la fuerza
                    máxima, la velocidad en carrera o el trabajo realizado durante un ejercicio determinado
                    por ejemplo.</p>
                    <p>También podría un suplemento tener un valor ergogénico si mejora la capacidad de un
                    atleta para realizar un ejercicio en especifico o mejora su capacidad de recuperación de
                    cara a un ejercicio.</p>
                    <p>Sin embargo, la ISSN ha decidido adoptar una posición más amplia sobre el valor
                    ergogénico de los suplementos asegurando de que si bien la construcción muscular y
                    los efectos en el rendimiento sobre un ejercicio puede generar efectos ergogénicos o
                    adaptaciones, dicha evidencia no justifica una evidencia excepcional para respaldar su
                    eficacia.</p>
                    <p>La ISSN define a un suplemento que es claramente ergogénico cuando la mayor parte
                    de los estudios respalda al ingrediente efectivo en promover la hipertrofia muscular o el
                    rendimiento en el entrenamiento.
                    </p>
                    <p>Y suplementos que no cumplen con esa normativa y solo están respaldados por datos
                    preclínicos , como son los estudios en ratas, se deben agrupar en una categoría
                    distinta.</p>
                    <h2 style={{margin: '40px 0'}}>Categorización y clasificación de los suplementos</h2>
                    <p>Debes tener en cuenta que los suplementos pueden contener:</p>
                    <p>Carbohidratos, proteínas, grasas, minerales, hierbas, enzimas, aminoácidos
                    seleccionados o extractos de plantas y alimentos.</p>
                    <p>Los suplementos deportivos se pueden clasificar como complementos de conveniencia.</p>
                    <p>Por ejemplo: Barras energéticas, geles, sustitutos de comida en polvo o suplementos
                    en bebida.</p>
                    <p>Que están diseñados para proporcionar un medio conveniente para satisfacer las
                    necesidades de energía o de macronutrientes que se necesitan.</p>
                    <p>Al mismo tiempo que brindan apoyo para controlar el consumo de calorías, aumento de
                    peso, pérdida de peso y/o mejora en el rendimiento deportivo.</p>
                    <p>Cómo hemos afirmado antes, evaluar la literatura científica disponible para cada uno de
                    esos elementos es muy importante para determinar la eficacia de cualquier suplemento
                    dietético.</p>
                    <p>Considerando esto se pueden clasificar a los suplementos nutricionales de la siguiente
                    forma:</p>
                    <h3 style={{margin: '40px 0'}}>Evidencia I. Fuerte evidencia para respaldar la eficacia y la aparente
                    seguridad del suplemento.</h3>
                    <p>En esta categoría se ubican los suplemento que tienen un fundamento teórico sólido
                    con la mayoría de las investigaciones disponibles en poblaciones relevantes
                    usando regímenes de dosis apropiadas demostrando tanto su eficacia como su
                    seguridad.</p>
                    <h3 style={{margin: '40px 0'}}>Evidencia II. Evidencia limitada o mixta para respaldar la eficacia del
                    suplemento.</h3>
                    <p>Suplementos que se ubican dentro de esta categoría se caracterizan por tener una
                    base sólida científica para su uso, pero los estudios disponibles no han logrado
                    resultados consistentes que respalden su eficacia.</p>
                    <p>Estos suplementos requieren de más estudios antes de que los investigadores puedan
                    comprender su verdadero impacto. Es importante destacar que estos suplementos no
                    tienen evidencia disponible suficiente para sugerir que carecen de seguridad o que
                    deben considerarse dañinos.</p>
                    <h3 style={{margin: '40px 0'}}>Evidencia III. Poca o ninguna evidencia que respalde su eficacia y/o
                    seguridad.</h3>
                    <p>Los suplementos deportivos dentro de esta categoría carecen de una base sólida
                    científica y los estudios disponibles muestra de forma consistente que carecen de
                    eficacia.</p>
                    <p>También entran en esta categoría suplementos que pueden ser perjudiciales para la
                    salud o no son seguros.</p>
                    <h2 style={{margin: '40px 0'}}>Conclusión sobre los suplementos deportivos</h2>
                    <p>Muchos factores se deben evaluar antes de aconsejar a quienes realizan ejercicio de
                    forma regular.</p>
                    <p>Es muy importante primero tener una comprensión clara de los objetivos del atleta y el
                    tiempo en el que se plantea conseguir dichos objetivos.</p>
                    <p>Además para poder monitorear la carga y la recuperación se debe hacer una
                    evaluación de la dieta y el programa de entrenamiento del individuo.</p>
                    <p>Para lograr esto se debe asegurar que el atleta está llevando a cabo una alimentación
                    balanceada, densa en nutriente y logra cubrir sus requerimientos calóricos diarios. Y
                    que además está entrenando de forma inteligente con una rutina planificada.</p>
                    <p>Muchos atletas y entrenadores están muy enfocados en la suplementación y se olvidan
                    y dejan de un lado esos aspectos fundamentales, pilares del rendimiento físico
                    deportivo.</p>
                    <p>Una vez se tengan esos aspectos controlados se sugiere que solo se recomienden
                    suplemento en la categoría I con fuerte evidencia que respalde su eficacia y su
                    aparente seguridad.</p>
                    <p>Si un atleta está interesado en probar suplementos de la categoría II, con evidencia
                    mixta o limitada que respalde su eficacia, debe asegurarse de que entiende que estos
                    suplementos nutricionales son más experimentales y que pueden o no ver los
                    resultados que se le atribuyen.</p>
                    <p>La ISSN no recomienda que los atletas consuman suplementos ubicados en la
                    categoría III, que tienen poca o ninguna evidencia para respaldar su eficacia y/o
                    seguridad.</p>
                    <p>Creemos que este enfoque está científicamente fundamentado y ofrece una visión
                    equilibrada en lugar de simplemente descartar el uso de todos los suplementos
                    dietéticos.</p>
                    <h2 style={{margin: '40px 0'}}>Referencias Bibliográficas</h2>
                    <ol>
                        <li>Kerksick, C.M., Wilborn, C.D., Roberts, M.D. et al. ISSN exercise and sports nutrition review update: 
                            research and recommendations. J Int Soc Sports Nutr 15, 38 (2018).
                            Disponible en:
                            <a className="links" href="https://jissn.biomedcentral.com/articles/10.1186/s12970-018-0242-y"> https://jissn.biomedcentral.com/articles/10.1186/s12970-018-0242-y</a></li>
                        <li>Bravo, V. Los 10 mejores (y peores) suplementos para bajar de peso comprobados científicamente.
                            FullMusculo (2020).
                            Disponible en:
                            <a className="links" href="https://fullmusculo.com/suplementos-para-bajar-de-peso/"> https://fullmusculo.com/suplementos-para-bajar-de-peso/</a></li>
                        <li>Bravo, V. 12 Suplementos para ganar masa muscular con evidencia científica.
                            FullMusculo (2019).
                            Disponible en:
                            <a className="links" href="https://fullmusculo.com/top-12-suplementos-para-aumentar-masa-muscular/"> https://fullmusculo.com/top-12-suplementos-para-aumentar-masa-muscular/</a></li>
                        <li>Bravo, V. 11 Suplementos que NO funcionan (con evidencia científica).
                            FullMusculo (2019).
                            Disponible en:
                            <a className="links" href="https://fullmusculo.com/suplementos-sin-evidencia-cientifica/"> https://fullmusculo.com/suplementos-sin-evidencia-cientifica/</a></li>
                    </ol>
                </>
                :
                    <div style={{display: 'flex',justifyContent: 'center'}}>
                        <Button onClick={() => this.setState({showmore: false, visible: true})} className="btnshow">Seguir leyendo</Button>
                    </div>
                }
                </div>
                <div className={classnames("toup", {
                    "toup--hidden": !this.state.visible
                    })}>
                    <ScrollUpButton StopPosition={280} ContainerClassName="ScrollUpButton__Container" TransitionClassName="ScrollUpButton__Toggled">
                        <Button className="toup" variant="primary"><i className="fas fa-arrow-up"></i></Button>
                    </ScrollUpButton>
                </div>
            </div>        
        )
    }
}

export default TextContent