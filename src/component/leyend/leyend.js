import React, {Component}from 'react';
import './leyend.css'

class Leyend extends Component {

    render() {
        return (
            <div style={{justifyContent: 'space-between',display: 'flex',flexFlow: 'row'}}>
                <div className="extra-text">
                    <p>Investigación: Victor Bravo @themedifitness</p>
                    <p>Fuentes: ISSN (Internacional Society of Sports Nutrition), PubMed</p>
                </div>
                <div className="leyendcontent">
                    <div className="row icons">
                        <div className="rounded r1"></div>
                        <div className="rounded r2"></div>
                        <div className="rounded r3"></div>
                        <div className="rounded r4"></div>
                    </div>
                    <label>Popularidad</label> 
                </div>
            </div>
           
        );
    }
}

export default Leyend