import React, {Component} from 'react';
import PropTypes from "prop-types";
import suplementos from '../../../models/suplementos'
import logo from '../../../assets/img/logoHeader.svg'
import svg from '../../../assets/svg/three-dots.svg'
import {Modal, Button} from 'react-bootstrap'
import './evidences.css'
class EvidenceII extends Component { 
    constructor(props, context) {
        super(props, context);
    
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    
        this.state = {
            show: false,
            nombre: 'nombre',
            nombre_producto: '',
            link: '',
            descripcion: '',
            posts: [],
            price: '',
            img: '',
            width: window.innerWidth
        };
    }
    handleClose() {
        this.setState({ show: false });
        this.setState({price: ''})
        this.setState({img: ''})
    }

    handleShow(suplemento) {
        if (suplemento.action) {
            this.setState({ show: true });
            this.setState({ nombre: suplemento.nombre})
            this.setState({ nombre_producto: suplemento.nombre_producto})
            this.setState({ descripcion: suplemento.descripcion})
            this.setState({ posts: suplemento.posts})
            if (localStorage.getItem('country') !== 'Spain') {
                this.setState({ link: suplemento.link_u})
                this.setState({ AssociateTag: 'fullmusculo0c-20'})
                this.setState({ img: suplemento.imagen })
                this.setState({ price: suplemento.price.usd })
                /* axios.get('https://fullmusculo.com/api/aws?domain=webservices.amazon.com&tag=fullmusculo0c-20&asin=' + suplemento.asin_u)
                .then((response) => {
                    console.log(response.data.data)
                    this.setState({price: response.data.data.price})
                    this.setState({img: response.data.data.image})
                }).catch((err) => {
                    console.log(err)
                }) */
            } else {
                this.setState({ link: suplemento.link_es})
                this.setState({ AssociateTag: 'fullm-21' })
                this.setState({ img: suplemento.imagen })
                this.setState({ price: suplemento.price.eur })
                /* axios.get('https://fullmusculo.com/api/aws?domain=webservices.amazon.es&tag=fullm-21&asin='+ suplemento.asin_es)
                .then((response) => {
                    console.log(response)
                    this.setState({price: response.data.data.price})
                    this.setState({img: suplemento.imagen? suplemento.imagen:response.data.data.image})
                }).catch((err) => {
                    console.log(err)
                }) */
            }
        }
    }

    getTypeOfPopularity(value) {
        var p = value[0] !== 0 ? value[0]:(value[1] !== 0 ? value[1]:value[2])
        switch (p) {
            case 1: return 'p1'; 
            case 2: return 'p2'; 
            case 3: return 'p3'; 
            case 4: return 'p4'; 
            default: break;
        }
    }
    getMargin(value) {
        var p = value[0] !== 0 ? value[0]:(value[1] !== 0 ? value[1]:value[2])
        return p;
    }
    showtext(value) {
        return value !== 0
    }

    setColor(value) {
        switch (value) {
            case 0: return '#700961';
            case 1: return '#b80d57';
            case 2: return '#e03e36';
            default: break;
        }
    }
    render() {
        const menu = this.props.menuselect
        return(
            <div className="evidenceI">
                <div className="row">
                    {
                        suplementos.map ( (suplemento,i) => 
                            <>
                            {
                                /*  suplemento.eficiencia[menu] === 1 */
                                (suplemento.eficiencia[0] === 2 || suplemento.eficiencia[1] === 2 || suplemento.eficiencia[2] === 2)  ? 
                                    <div 
                                        className={"suplements " + this.getTypeOfPopularity(suplemento.popularidad)} 
                                        style={{
                                            marginTop: this.getMargin(suplemento.evidencia)+ '%',
                                            backgroundColor: (this.showtext(suplemento.popularidad[menu]) && suplemento.eficiencia[menu] === 2 ? this.setColor(menu):null),
                                            color: (this.showtext(suplemento.popularidad[menu]) ? 'white': null),
                                            display: (this.state.width < 992 && (this.showtext(suplemento.popularidad[menu])) && suplemento.eficiencia[menu] === 2) ? null : this.state.width > 992 ? null:'none'
                                        }}
                                        key={suplemento.nombre} 
                                        onClick={() => {
                                            if (this.showtext(suplemento.popularidad[menu])) {
                                                this.handleShow(suplemento)
                                            }
                                        }}
                                    >
                                        {this.showtext(suplemento.popularidad[menu]) ? <p>{suplemento.nombre}</p>:null}    
                                    </div>
                                : null
                            }
                            </>
                        )
                    }
                </div>
                <Modal  size="lg" show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Body style={{ marginBottom: '20px' }}>

                        {
                            this.state.price !== 'NO DISPONIBLE' ?
                                <>
                                    <div className="row">
                                        <div className="col-lg-6">
                                            {
                                                this.state.img ?
                                                    <img className="product-img" src={this.state.img} alt={this.state.nombre_producto}></img> :
                                                    <img className="loader" src={svg} alt="Cargando"></img>
                                            }
                                        </div>
                                        <div className="col-lg-6">
                                            <h5>{this.state.nombre_producto}</h5>
                                            {
                                                this.state.descripcion.split('\n').map(i => {
                                                    return <p style={{ marginBottom: '0em' }}>{i}</p>
                                                })
                                            }
                                            <br></br>
                                            {
                                                this.state.price ?
                                                    <p className="price">{this.state.price}</p> :
                                                    <img className="loader" src={svg} alt="Cargando"></img>
                                            }
                                            <Button onClick={() => window.open(this.state.link, "_blank")} className="shop">Comprar</Button>
                                        </div>
                                    </div>
                                    {
                                        this.state.posts.length > 0 ?
                                            <div style={{ marginTop: '40px' }}>
                                                <p style={{ color: 'darkgray' }}>Posts relacionados:</p>
                                                <div className="row">
                                                    {
                                                        this.state.posts.map(post =>
                                                            <div className="col-12">
                                                                <p onClick={() => window.open(post.link, "_blank")} className="post">{post.nombre}</p>
                                                            </div>
                                                        )
                                                    }
                                                </div>
                                            </div>
                                            : null
                                    }
                                </> :
                                <>
                                    <div className="container" style={{ textAlign: 'center' }}>
                                        <img src={logo} alt="" style={{ width: '25%' }}></img>
                                        <h5>Disculpe, en estos momentos no tenemos este producto disponible</h5>
                                    </div>
                                </>
                        }


                    </Modal.Body>
                </Modal>
            </div>
            
        );
    }
}
EvidenceII.propTypes = {
    menuselect: PropTypes.number
};
EvidenceII.defaultProps = {
    menuselect: 1
};
export default EvidenceII;