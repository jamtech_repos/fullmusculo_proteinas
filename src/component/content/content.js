import React, {Component} from 'react'
import PropTypes from "prop-types";
import EvidenceI from './evidences/eI'
import EvidenceII from './evidences/eII'
import EvidenceIII from './evidences/eIII'

import Leyend from '../leyend/leyend'
import './content.css'


class Content extends Component {
    constructor(props, context) {
        super(props, context);
    
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        
        this.state = {
          show: false
        };
    }
    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }
    render() {
        const menu = this.props.menuselect
        return (
            <div className="container-fluid">
            {/* <Button variant="primary" onClick={this.handleShow}>
                Launch demo modal
            </Button>

                 */}
                
                <div className="board">
                    <div className="leyendae">
                        <div className="plus"><label>+</label></div>
                        <div class="arrowTop"></div>
                        <p className="ver">
                            Efectividad / Evidencia
                        </p>
                        <div class="arrowBottom"></div>
                        <div className="minus"><label>-</label></div>
                    </div>
                    
                    <div id="evidencias">
                        <EvidenceI menuselect={menu}/>
                        <p className="evidencia-line">Evidencia I</p>
                        <EvidenceII menuselect={menu}/>
                        <p className="evidencia-line">Evidencia II</p>
                        <EvidenceIII menuselect={menu}/>
                        <p className="evidencia-line">Evidencia III</p>
                    </div>
                    
                </div>
                <Leyend />
                
            </div>
        )
    }
}

Content.propTypes = {
    menuselect: PropTypes.number
};
Content.defaultProps = {
    menuselect: 0
};

export default Content 