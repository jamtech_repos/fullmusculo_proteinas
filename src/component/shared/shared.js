import React, {Component}from 'react';
import {FacebookShareButton, TwitterShareButton, PinterestShareButton} from 'react-share'
import { Button } from 'react-bootstrap'
import suplementos from '../../assets/img/suplementos.jpg';
import suplementos2 from '../../assets/img/suplementos2.jpg';
import exampleImage from '../../assets/img/example.png'
import './shared.css'
class Shared extends Component {

    render() {
        const shareUrl = 'https://fullmusculo.com/suplementos-deportivos/';
        const title = 'La guia de suplementos de fullmusculo';
        const titletw = 'Los Mejores suplementos deportivos según su evidencia científica, vía @fullmusculo'
        return (
            <div className="container" style={{marginTop:'75px'}}>
                <h3 style={{textAlign: "center"}}>Comparte esta gráfica</h3>
                <div className="row" style={{justifyContent: "center"}}>
                    <FacebookShareButton url={shareUrl} quote={title}>
                        <Button className="sharedbtn fb" >
                            <i className="fab fa-facebook-f" style={{marginRight: "15px"}}></i>Facebook
                        </Button>
                    </FacebookShareButton>
                    <TwitterShareButton url={shareUrl} title={titletw}>
                        <Button className="sharedbtn tw" >
                            <i className="fab fa-twitter " style={{marginRight: "15px"}}></i>Twitter
                        </Button>
                    </TwitterShareButton>
                    <PinterestShareButton  url={String(window.location)}
                        media={'https://fullmusculo.com' + exampleImage}
                        description={titletw}
                        windowWidth={1000}
                        windowHeight={730} quote={title}>
                        <Button className="sharedbtn pt" >
                            <i className="fab fa-pinterest-p" style={{marginRight: "15px"}}></i>Pinterest
                        </Button>
                    </PinterestShareButton>
                </div>
                <div style={{ margin: '75px auto'}} className="container">
                    <h3>Post sobre suplementos:</h3>
                    <div  className="row">
                        <div onClick={() => window.open('https://fullmusculo.com/suplementos-para-bajar-de-peso/', "_blank")} className="container post col-12 col-lg-4">
                            <img style={{ width: '100%' }} src={suplementos} alt="Los 10 Mejores suplementos para bajar de peso"></img>
                            <p style={{marginTop:'20px'}} className="post">Los 10 Mejores suplementos para bajar de peso</p>
                        </div>
                        <div onClick={() => window.open('https://fullmusculo.com/top-12-suplementos-para-aumentar-masa-muscular/', "_blank")} className="container post col-12 col-lg-4">
                            <img style={{ width: '100%' }} src={suplementos} alt="Los 12 Mejores suplementos para Aumentar Masa Muscular"></img>
                            <p style={{ marginTop: '20px' }} className="post">Los 12 Mejores suplementos para Aumentar Masa Muscular</p>
                        </div>
                        <div onClick={() => window.open('https://fullmusculo.com/suplementos-sin-evidencia-cientifica/', "_blank")} className="container post col-12 col-lg-4">
                            <img style={{ width: '100%' }} src={suplementos2} alt="Los 11 Suplementos que no funcionan"></img>
                            <p style={{ marginTop: '20px' }} className="post">Los 11 Suplementos que no funcionan</p>
                        </div>
                    </div>
                </div>
               
                
            </div>
        );
    }
}

export default Shared