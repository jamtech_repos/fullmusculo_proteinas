import AminoacidosEsenciales from '../assets/img/suplementos/AminoacidosEsenciales.jpg'
import Arginina from '../assets/img/suplementos/Arginina.jpg'
import ATP from '../assets/img/suplementos/ATP.jpg'
import BCAA from '../assets/img/suplementos/BCAA.jpg'
import BebidaIsotonica from '../assets/img/suplementos/BebidaIsotonica.jpg'
import BetaAlanina from '../assets/img/suplementos/BetaAlanina.jpg'
import Bicarbonato from '../assets/img/suplementos/Bicarbonato.jpg'
import Cafeina from '../assets/img/suplementos/Cafeina.jpg'
import CarbohidratosMA from '../assets/img/suplementos/CarbohidratosMA.jpg'
import CitrulinaMalato from '../assets/img/suplementos/CitrulinaMalato.jpg'
import CLA from '../assets/img/suplementos/CLA.jpg'
import Creatina from '../assets/img/suplementos/Creatina.jpg'
import Ginseng from '../assets/img/suplementos/Ginseng.jpg'
import Glutamina from '../assets/img/suplementos/Glutamina.jpg'
import HMB from '../assets/img/suplementos/HMB.jpg'
import InhibidorDeMyostatina from '../assets/img/suplementos/InhibidorDeMyostatina.jpg'
import LCarnitina from '../assets/img/suplementos/LCarnitina.jpg'
import Maltodextrina from '../assets/img/suplementos/Maltodextrina.jpg'
import Melatonina from '../assets/img/suplementos/Melatonina.jpg'
import Multivitaminico from '../assets/img/suplementos/Multivitaminico.jpg'
import PicolinatoDeCromo from '../assets/img/suplementos/PicolinatoDeCromo.jpg'
import ProteinaVegana from '../assets/img/suplementos/ProteinaVegana.jpg'
import Sinefrina from '../assets/img/suplementos/Sinefrina.jpg'
import Taurina from '../assets/img/suplementos/Taurina.jpg'
import TeVerde from '../assets/img/suplementos/TeVerde.jpg'
import Tribulus from '../assets/img/suplementos/Tribulus.jpg'
import WheyProtein from '../assets/img/suplementos/WheyProtein.jpeg'
import YerbaMate from '../assets/img/suplementos/YerbaMate.jpg'


export default {
    AminoacidosEsenciales,
    Arginina,
    ATP,
    BCAA,
    BebidaIsotonica,
    BetaAlanina,
    Bicarbonato,
    Cafeina,
    CarbohidratosMA,
    CitrulinaMalato,
    CLA,
    Creatina,
    Ginseng,
    Glutamina,
    HMB,
    InhibidorDeMyostatina,
    LCarnitina,
    Maltodextrina,
    Melatonina,
    Multivitaminico,
    PicolinatoDeCromo,
    ProteinaVegana,
    Sinefrina,
    Taurina,
    TeVerde,
    Tribulus,
    WheyProtein,
    YerbaMate
}