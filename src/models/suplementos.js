import imagenes from './imagenes'
 

const suplementos = [    
    {
        nombre: 'ATP',
        descripcion: ``,
        nombre_producto: 'ATP',
        evidencia: [0,7,0],
        eficiencia: [2,0,0],
        popularidad: [3,0,0],
        link_u: 'https://amzn.to/2QhqwEW',
        link_es: 'https://amzn.to/3qTa7Yr',
        asin_u: 'B0184N0644',
        asin_es: 'B00K28UY7E',
        action: true,
        posts: [],
        imagen: imagenes.ATP,
        price: {
            usd: '$20.89',
            eur: '27,90 €'
        }
    },
    {
        nombre: 'Ginseng',
        descripcion: `Estimulante natural para mejorar el rendimiento deportivo`,
        nombre_producto: 'Ginseng Koreano',
        evidencia: [0,0,0],
        eficiencia: [0,0,2],
        popularidad: [0,0,2],
        link_u: 'https://amzn.to/30HfTQk',
        link_es: 'https://amzn.to/3oba60d',
        asin_u: 'B01MECVTDY',
        asin_es: 'B015KHH22Q',
        action: true,
        posts: [
            {
                nombre:  'Propiedades y beneficios del Ginseng',
                link: 'https://fullmusculo.com/propiedades-beneficios-ginseng/',
            }
        ],
        imagen: imagenes.Ginseng,
        price: {
            usd: '$21.97',
            eur: '5,97 €'
        }
    },
    
    
    {
        nombre: 'Inhibidores de miostatina',
        descripcion: ``,
        nombre_producto: 'Inhibidor de miostatina en cápsulas',
        evidencia: [0,0,0],
        eficiencia: [3,0,0],
        popularidad: [4,0,0],
        link_u: 'https://amzn.to/2JXTxV2',
        link_es: 'https://amzn.to/2VZHDkH',
        asin_u: 'B00LLIK8Y8',
        asin_es: 'B00YURNAME',
        action: true,
        posts: [],
        imagen: imagenes.InhibidorDeMyostatina,
        price: {
            usd: '$39.99',
            eur: '36,90 €'
        }
    },
    {
        nombre: 'Arginina',
        descripcion: `
            Aminoácido no esencial\n
            Participa en el metabolismo muscular\n
            Contiene la forma libre del aminoácido para mejorar su absorción\n
            Precursor de Oxido nítrico
        `,
        nombre_producto: 'Arginina',
        evidencia: [0,0,0],
        eficiencia: [0,3,0],
        popularidad: [0,3,0],
        link_u: 'https://amzn.to/2YLBXaR',
        link_es: 'https://amzn.to/3qN5Fu7',
        asin_u: 'B0013OVX3U',
        asin_es: 'B0013OVX3U',
        action: true,
        posts: [
            {
                nombre:  'Alimentos ricos en Arginina',
                link: 'https://fullmusculo.com/alimentos-ricos-arginina/',
            },
            {
                nombre:  '¿Para qué sirve el óxido nítrico?',
                link: 'https://fullmusculo.com/oxido-nitrico/',
            }
        ],
        imagen: imagenes.Arginina,
        price: {
            usd: '$15.10',
            eur: '22,90 €'
        }
    },
    {
        nombre: 'Tribulus',
        descripcion: `
            Tabletas de tribulus terrestris con un alto contenido de saponinas\n
            90 tabletas (90 serv)\n
            Tomar 1 tableta al día con líquido
        `,
        nombre_producto: 'Tribulus Terrestris',
        evidencia: [5,0,0],
        eficiencia: [3,0,0],
        popularidad: [2,0,0],
        link_u: 'https://amzn.to/2QlM4QU',
        link_es: 'https://amzn.to/366i4Bq',
        asin_u: 'B01B6WXBB0',
        asin_es: 'B00XPNWJTO',
        action: true,
        posts: [{
            nombre: 'Beneficios y propiedades del Tribulus Terrestris',
            link: 'https://fullmusculo.com/beneficios-y-propiedades-del-tribulus-terrestris/'
        }],
        imagen: imagenes.Tribulus,
        price: {
            usd: '$10.00',
            eur: '12,75 €'
        }
    },
    {
        nombre: 'Picolinato de cromo',
        descripcion: `
            Favorece el metabolismo\n
            Ayuda al mantenimiento del nivel de glucosa en sangre\n
            Oligoelemento esencial
        `,
        nombre_producto: 'Picolinato de cromo en cápsulas vegetales',
        evidencia: [0,0,0],
        eficiencia: [0,0,3],
        popularidad: [0,0,3],
        link_u: 'https://amzn.to/2Qkqr32',
        link_es: 'https://amzn.to/3ca0Kzw',
        asin_u: 'B001BCPWJK',
        asin_es: 'B00020IC4O',
        action: true,
        posts: [],
        imagen: imagenes.PicolinatoDeCromo,
        price: {
            usd: '$9.90',
            eur: '17,86 €'
        }
    },
    {
        nombre: 'Glutamina',
        descripcion: `
            Glutamina pura en polvo\n
            Sin sabor\n
            Soluble en agua\n
            Se puede tomar antes, durante o después del ejercicio
        `,
        nombre_producto: 'Glutamina en polvo',
        evidencia: [0,0,2],
        eficiencia: [3,0,0],
        popularidad: [1,0,0],
        link_u: 'https://amzn.to/2Ern45W',
        link_es: 'https://amzn.to/3sNiQwS',
        asin_u: 'B002DYIZCQ',
        asin_es: 'B085LKD647',
        action: true,
        posts: [{
            nombre: '¿Cómo Tomar Glutamina para ganar masa muscular?',
            link: 'https://fullmusculo.com/glutamina-para-ganar-masa-muscular/'
        }],
        imagen: imagenes.Glutamina,
        price: {
            usd: '$10.85',
            eur: '15,90 €'
        }
    },
    {
        nombre: 'GHRP', /* péptido liberador de la Hormona de Crecimiento */
        descripcion: ``,
        nombre_producto: '',
        evidencia: [0,0,0],
        eficiencia: [3,0,0],
        popularidad: [4,0,0],
        link_u: '',
        link_es: '',
        asin_u: '',
        asin_es: '',
        action: true,
        posts: [],
        price: {
            usd: 'NO DISPONIBLE',
            eur: 'NO DISPONIBLE'
        }
    },
    {
        nombre: 'Alfa cetoglutarato',
        descripcion: ``,
        nombre_producto: '',
        evidencia: [0,0,0],
        eficiencia: [3,0,0],
        popularidad: [4,0,0],
        link_u: '',
        link_es: '',
        asin_u: '',
        asin_es: '',
        action: true,
        posts: [],
        price: {
            usd: 'NO DISPONIBLE',
            eur: 'NO DISPONIBLE'
        }
    },
    {
        nombre: 'Cafeína',
        descripcion: `
            Cafeína sin azúcar añadida ni calorías.\n
            1 Servicio 1 cápsula.\n
            Aumenta los reflejos y los niveles energéticos\n
            No GMO, Libre de Gluten.        
        `,
        nombre_producto: 'Pastillas de cafeína de 200 mg',
        evidencia: [0,0,0],
        eficiencia: [0,1,1],
        popularidad: [0,2,2],
        link_u: 'https://amzn.to/2YLBJAn',
        link_es: 'https://amzn.to/3a2xsQA',
        asin_u: 'B01DFQSVK6',
        asin_es: 'B01BCCOGX6',
        action: true,
        posts: [
            {
                nombre: 'Propiedades y beneficios de la cafeína',
                link: 'https://fullmusculo.com/cafeina-propiedades-y-beneficios/',
            }
        ],
        imagen: imagenes.Cafeina,
        price: {
            usd: '$17.95',
            eur: '9,90 €'
        }
    },
    {
        nombre: 'Beta alanina',
        descripcion: `
            En polvo sin añadidos\n
            Reduce la sensación de fatiga\n
            Ayuda a incrementar la fuerza y el volumen de entrenamiento        
        `,
        nombre_producto: 'Beta Alanina en polvo',
        evidencia: [0,-2,0],
        eficiencia: [0,1,0],
        popularidad: [0,3,0],
        link_u: 'https://amzn.to/2WtfYIt',
        link_es: 'https://amzn.to/3c9yNI4',
        asin_u: 'B00EIO4I7A',
        asin_es: 'B071KPSP9X',
        action: true,
        posts: [],
        marginright: '-5px',
        movilmarginTop: '9%',
        imagen: imagenes.BetaAlanina,
        price: {
            usd: '$18.01',
            eur: '22,90 €'
        }
    },
    {
        nombre: 'Bicarbonato de sodio',
        descripcion: `
            Sin sabor, en polvo y ecológico\n
            Apto para vegetarianos y veganos\n
            Libre de gluten, aluminio, agentes transgénicos y antiaglomerantes
        `,
        nombre_producto: 'Bicarbonato de sodio',
        evidencia: [0,-6,0],
        eficiencia: [0,1,0],
        popularidad: [0,3,0],
        link_u: 'https://amzn.to/2HBMnnX',
        link_es: 'https://amzn.to/2Zc8iLi',
        asin_u: 'B075456XMK',
        asin_es: 'B07417N3PW',
        action: true,
        posts: [
            {
                nombre: 'Propiedades del bicarbonato de sodio',
                link: 'https://fullmusculo.com/propiedades-del-bicarbonato-de-sodio/',
            }
        ],
        movilmarginleft: '-10%',
        marginright: '-55px',
        imagen: imagenes.Bicarbonato,
        price: {
            usd: '$12.95',
            eur: '10,75 €'
        }

    },
    {
        nombre: 'CHO de máxima absorción',
        descripcion: `
            Maltodextrina\n
            Sin sabor, para mezclar con agua o bebida deportiva\n
            Mejora el rendimiento deportivo        
        `,
        nombre_producto: 'Carbohidrato de máxima absorción',
        evidencia: [0,7,0],
        eficiencia: [0,1,0],
        popularidad: [0,4,0],
        link_u: 'https://amzn.to/2WjElbb',
        link_es: 'https://amzn.to/3a0fUoa',
        asin_u: 'B0013OUNRM',
        asin_es: 'B072RK6DKT',
        action: true,
        posts: [
            {
                nombre: '¿Carbohidratos antes y después de entrenar?',
                link: 'https://fullmusculo.com/carbohidratos-antes-y-despues-de-entrenar/',
            },
            {
                nombre: 'Carbohidratos post entrenamiento para la recuperación muscular',
                link: 'https://fullmusculo.com/carbohidratos-post-entrenamiento/',
            }
        ],
        marginleft: '-10%',
        movilmarginTop: '15%',
        movilmarginleft: '-7%',
        imagen: imagenes.CarbohidratosMA,
        price: {
            usd: '$25.20',
            eur: '8,41 €'
        }
    },
    
    {
        nombre: 'Proteína vegana',
        descripcion: 'Proteína a base de componentes vegetales\n'+
            'Se adapta perfectamente a dietas veganas y vegetarianas al no contener componente animal.\n'+
            'Mezclar con agua o leche de preferencia.',
        nombre_producto: 'Proteína vegana',
        evidencia: [8,0,0],
        eficiencia: [1,0,0],
        popularidad: [2,0,0],
        link_u: 'https://amzn.to/2K59kBc',
        link_es: 'https://amzn.to/3sRhsJD',
        asin_u: 'B00J074W7Q',
        asin_es: 'B016YWDAAY',
        action: true,
        posts: [],
        marginright: '-2%',
        imagen: imagenes.ProteinaVegana,
        price: {
            usd: '$30.93',
            eur: '19,99 €'
        }
    },
    {
        nombre: 'Proteina de suero',
        descripcion: `Aislado de proteína de suero con un porcentaje superior al 90% de proteína pura\n
            24 gramos de proteína por cada porción\n
            5 gramos de BCAA por porción`,
        nombre_producto: 'Proteína de suero en polvo Optimum Nutrition',
        evidencia: [0,0,0],
        eficiencia: [1,0,0],
        popularidad: [1,0,0],
        link_u: 'https://amzn.to/2YRbXLt',
        link_es: 'https://amzn.to/3c7vqBe',
        asin_u: 'B000QSNYGI',
        asin_es: 'B000GISTZ4',
        action: true,
        posts: [
            {
                nombre:  'Los beneficios de la proteína de suero de leche',
                link: 'https://fullmusculo.com/whey-protein-los-beneficios-de-la-proteina-de-suero-de-leche/',
            }
        ],
        imagen: imagenes.WheyProtein,
        price: {
            usd: '$55.04',
            eur: '29,90 €'
        }
    },
    {
        nombre: 'Creatina',
        descripcion: `Sin sabor\n
            Incrementa la energía y la recuperación\n
            El suplemento con más evidencia científica del mercado
        `,
        nombre_producto: 'Creatina Monohidratada en polvo',
        evidencia: [0,0,0],
        eficiencia: [1,1,0],
        popularidad: [2,2,0],
        link_u: 'https://amzn.to/2WtfwKh',
        link_es: 'https://amzn.to/39ephkK',
        asin_u: 'B002DYIZEO',
        asin_es: 'B00T7L20EC',
        action: true,
        posts: [
            {
                nombre:  '¿Qué es la creatina, para que sirve y cómo tomarla?',
                link: 'https://fullmusculo.com/lo-que-debes-saber-sobre-la-creatina/',
            }
        ],
        imagen: imagenes.Creatina,
        price: {
            usd: '$14.33',
            eur: '18,91 €'
        }
    },
    {
        nombre: 'Aminoacidos esenciales',
        descripcion: `
            Tabletas de aminoácidos esenciales\n
            Contienen una relación de 2:1:1 de lecitina, isoleucina y valina
        `,
        nombre_producto: 'Aminoácidos esenciales',
        evidencia: [9,0,0],
        eficiencia: [0,2,0],
        popularidad: [0,2,0],
        link_u: 'https://amzn.to/2M7ecbX',
        link_es: 'https://amzn.to/3pmAEgs',
        asin_u: 'B003X5LM1U',
        asin_es: 'B0055BYEBU',
        action: true,
        posts: [],
        marginright: '-40px',
        marginleft: '-50px',
        movilmarginTop: '20%',
        movilmarginleft: '-10%',
        imagen: imagenes.AminoacidosEsenciales,
        price: {
            usd: '$18.99',
            eur: '18,21 €'
        }
    },
    {
        nombre: 'Yerba mate',
        descripcion: `
            Presentación de 1 Kilo\n
            Té herbal tradicional en Sur América\n
            Te da energía y promueve la perdida de peso        
        `,
        nombre_producto: 'Yerba Mate',
        evidencia: [0,0,0],
        eficiencia: [0,0,1],
        popularidad: [0,0,2],
        link_u: 'https://amzn.to/2EncH31',
        link_es: 'https://amzn.to/2EEioKb',
        asin_u: 'B00E3UI0SC',
        asin_es: 'B002ZBBTLG',
        action: true,
        posts: [],
        imagen: imagenes.YerbaMate,
        price: {
            usd: '$12.05',
            eur: '13,90 €'
        }
    },
    {
        nombre: 'HMB',
        descripcion: `
            Ingredientes 100% puros\n
            Aumenta el rendimiento y disminuye el catabolismo muscular
        `,
        nombre_producto: 'HMB',
        evidencia: [11,0,0],
        eficiencia: [1,0,0],
        popularidad: [4,0,0],
        link_u: 'https://amzn.to/2JYT84M',
        link_es: 'https://amzn.to/3iLeQso',
        asin_u: 'B000GIPJ16',
        asin_es: 'B00JKJBJJ8',
        action: true,
        posts: [
            {
                nombre:  '¿Qué es el HMB y funciona para aumentar masa muscular?',
                link: 'https://fullmusculo.com/hmb-para-aumentar-musculo/',
            }
        ],
        marginleft: '-5%',
        imagen: imagenes.HMB,
        price: {
            usd: '$23.08',
            eur: '28,20 €'
        }
    },
    {
        nombre: 'Té verde',
        descripcion: `
            Auténtico japones\n
            100% orgánico\n
            Rico en catequina
        `,
        nombre_producto: 'Té verde',
        evidencia: [0,0,0],
        eficiencia: [0,0,1],
        popularidad: [0,0,1],
        link_u: 'https://amzn.to/2QiQojD',
        link_es: 'https://amzn.to/3pi07av',
        asin_u: 'B00PFDH0IC',
        asin_es: 'B07R8715G1',
        action: true,
        posts: [
            {
                nombre:  'Beneficios del té verde',
                link: 'https://fullmusculo.com/beneficios-del-te-verde/',
            },
            {
                nombre:  '¿Cómo tomar té verde para adelgazar',
                link: 'https://fullmusculo.com/por-que-te-verde-para-perder-peso/',
            }
        ],
        marginleft: '-4px',
        imagen: imagenes.TeVerde,
        price: {
            usd: '$9.45',
            eur: '12,95 €'
        }
    },
    {
        nombre: 'Melatonina',
        descripcion: `
            Ayuda a recuperar el ciclo del sueño\n
            Relajante natural
        `,
        nombre_producto: 'Melatonina en cápsulas',
        evidencia: [0,9,0],
        eficiencia: [0,1,0],
        popularidad: [0,4,0],
        link_u: 'https://amzn.to/2M5eu2M',
        link_es: 'https://amzn.to/2LUKuaS',
        asin_u: 'B003KLROVY',
        asin_es: 'B00DN9H5TY',
        action: true,
        posts: [],
        marginleft: '-2%',
        movilmarginleft: '-6%',
        movilmarginTop: '23%',
        imagen: imagenes.Melatonina,
        price: {
            usd: '$9.36',
            eur: '11,26 €'
        }
    },
    {
        nombre: 'Bebida deportiva o isotonica',
        descripcion: `
            Multiples sabores\n
            Bebida para antes, durante y después de hacer deporte
        `,
        nombre_producto: 'Bebida deportiva o isotonica',
        evidencia: [0,5,0],
        eficiencia: [0,1,0],
        popularidad: [0,2,0],
        link_u: 'https://amzn.to/2yauIl7',
        link_es: 'https://amzn.to/3pi0v90',
        asin_u: 'B00HC767P6',
        asin_es: 'B0741967Y3',
        action: true,
        posts: [{
            nombre:  'La importancia de la hidración en el deporte',
            link: 'https://fullmusculo.com/hidratacion/',
        }],
        movilmarginTop: '5%',
        movilmarginleft: '-5%',
        imagen: imagenes.BebidaIsotonica,
        price: {
            usd: '$8.04',
            eur: '17,02 €'
        }
    },
    {
        nombre: 'BCAA',
        descripcion: `
            Aminoácidos esenciales de cadena ramificada\n
            Proporción de 2:1:1 de leucina, isoleucina y valina.\n
            Para la recuperación muscular
        `,
        nombre_producto: 'BCAA',
        evidencia: [0,0,0],
        eficiencia: [2,2,0],
        popularidad: [2,2,0],
        link_u: 'https://amzn.to/2JY03Lh',
        link_es: 'https://amzn.to/3t3X9ZM',
        asin_u: 'B000SOXALE',
        asin_es: 'B009OYC5LU',
        action: true,
        posts: [
            {
                nombre:  '¿Cómo tomar BCAA para Aumentar masa muscular?',
                link: 'https://fullmusculo.com/tomar-aminoacidos-bcaa-para-construir-musculo',
            }
        ],
        imagen: imagenes.BCAA,
        price: {
            usd: '$32.84',
            eur: '29,90 €'
        }
    },
    {
        nombre: 'Ácido fosfatidico',
        descripcion: ``,
        nombre_producto: '',
        evidencia: [10,0,0],
        eficiencia: [2,0,0],
        popularidad: [4,0,0],
        link_u: '',
        link_es: '',
        asin_u: '',
        asin_es: '',
        action: true,
        posts: [],
        price: {
            usd: 'NO DISPONIBLE',
            eur: 'NO DISPONIBLE'
        }
    },
    {
        nombre: 'Citrulina malato',
        descripcion: `
            Sin sabor\n
            Gran pureza y testeado\n
            Precursor del óxido nitrico.        
        `,
        nombre_producto: 'Citrulina Malato en polvo',
        evidencia: [0,5,0],
        eficiencia: [0,2,0],
        popularidad: [0,4,0],
        link_u: 'https://amzn.to/2QiLZgC',
        link_es: 'https://amzn.to/367f4F6',
        asin_u: 'B00EYDJTRE',
        asin_es: 'B073D1G2GZ',
        action: true,
        posts: [],
        imagen: imagenes.CitrulinaMalato,
        price: {
            usd: '$24.66',
            eur: '12,90 €'
        }
    },
    {
        nombre: 'Efedrina',
        descripcion: ``,
        nombre_producto: '',
        evidencia: [0,0,0],
        eficiencia: [0,0,2],
        popularidad: [0,0,3],
        link_u: '',
        link_es: '',
        asin_u: '',
        asin_es: '',
        action: true,
        posts: [],
        price: {
            usd: 'NO DISPONIBLE',
            eur: 'NO DISPONIBLE'
        }
    },
    {
        nombre: 'Taurina',
        descripcion: `
            Aminoácido esencial\n
            No GMO y libre de gluten\n        
        `,
        nombre_producto: 'Taurina',
        evidencia: [0,11,0],
        eficiencia: [0,2,0],
        popularidad: [0,4,0],
        link_u: 'https://amzn.to/2M7armH',
        link_es: 'https://amzn.to/3pkGK0E',
        asin_u: 'B01CUYHCX6',
        asin_es: 'B000RMH2C0',
        action: true,
        posts: [
            {
                nombre:  'Taurina: Beneficios, propiedades y contraindicaciones',
                link: 'https://fullmusculo.com/taurina/',
            }
        ],
        imagen: imagenes.Taurina,
        price: {
            usd: '$14.95',
            eur: '6,60 €'
        }
    },
    {
        nombre: 'Multivitamínicos',
        descripcion: `
            Una mezcla de más de 20 Vitaminas y minerales\n
            Una toma al día\n
            Provisión para todo un año
        `,
        nombre_producto: 'Multivitamínico',
        evidencia: [0,0,2],
        eficiencia: [0,3,0],
        popularidad: [0,2,0],
        link_u: 'https://amzn.to/2M4OswU',
        link_es: 'https://amzn.to/3qRobSi',
        asin_u: 'B079C73K45',
        asin_es: 'B008FN6YOU',
        action: true,
        posts: [
            {
                nombre:  'Todo lo que necesitas saber de los multivitamínicos',
                link: 'https://fullmusculo.com/multivitaminicos-todo-lo-que-necesitas-saber/',
            }
        ],
        imagen: imagenes.Multivitaminico,
        price: {
            usd: '$11.62',
            eur: '25,65 €'
        }
    },
    
   
    
    {
        nombre: 'Sinefrina',
        descripcion: `Promueve la pérdida de peso`,
        nombre_producto: 'Sinefrina en cápsulas',
        evidencia: [0,0,0],
        eficiencia: [0,0,2],
        popularidad: [0,0,2],
        link_u: 'https://amzn.to/2VKhxwQ',
        link_es: '',
        asin_u: 'B0060SZOY8',
        asin_es: '',
        action: true,
        posts: [],
        imagen: imagenes.Sinefrina,
        price: {
            usd: '$11.42',
            eur: 'NO DISPONIBLE'
        }
    },
    
    {
        nombre: 'CLA',
        descripcion: `Promueve la oxidación de grasa`,
        nombre_producto: 'Ácido linoleico conjugado en cápsulas blandas',
        evidencia: [0,0,0],
        eficiencia: [0,0,3],
        popularidad: [0,0,2],
        link_u: 'https://amzn.to/2MfhoCu',
        link_es: 'https://amzn.to/3pfvtPj',
        asin_u: 'B00HAS5BUY',
        asin_es: 'B00JV9JTN0',
        action: true,
        posts: [],
        imagen: imagenes.CLA,
        price: {
            usd: '$20.96',
            eur: '27,20 €'
        }
    },
    {
        nombre: 'L-Carnitina',
        descripcion: `Promueve el metabolismo de los ácidos grasos y la recuperación.`,
        nombre_producto: 'L-Carnitina en cápsulas',
        evidencia: [0,0,0],
        eficiencia: [0,0,3],
        popularidad: [0,0,1],
        link_u: 'https://amzn.to/2YLqmIG',
        link_es: 'https://amzn.to/3qOvA4K',
        asin_u: 'B01M9D1J2C',
        asin_es: 'B07PDPPGN9',
        action: true,
        posts: [{
            nombre: '¿Qué es la L-carnitina y funciona para quemar grasa?',
            link: 'https://fullmusculo.com/l-carnitina'
        }],
        imagen: imagenes.LCarnitina,
        price: {
            usd: '$19.95',
            eur: '15,90 €'
        }
    },
    
];

export default suplementos;