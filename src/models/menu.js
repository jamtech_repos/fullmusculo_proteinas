import energia from '../assets/svg/003-solar-energy.svg'
import calorias from '../assets/svg/004-calories.svg'
import muscular from '../assets/svg/002-muscle.svg'

const ArrayMenu = [
    {
        title: 'Ganar Músculos',
        svg: muscular,
        pos: 0,
        color: '#700961',
        filter: 'invert(8%) sepia(47%) saturate(6835%) hue-rotate(298deg) brightness(108%) contrast(101%)'
    },
    {
        title: 'Rendimiento Deportivo',
        svg: energia,
        pos: 1,
        color: '#b80d57',
        filter: 'invert(14%) sepia(87%) saturate(3705%) hue-rotate(321deg) brightness(92%) contrast(101%)'
    },
    {
        title: 'Perder Grasa',
        svg: calorias,
        pos: 2,
        color: '#e03e36',
        filter: 'invert(46%) sepia(77%) saturate(5681%) hue-rotate(343deg) brightness(92%) contrast(89%)'
    },
    {
        title: 'Ganar Músculos',
        svg: muscular,
        pos: 0,
        color: '#700961',
        filter: 'invert(8%) sepia(47%) saturate(6835%) hue-rotate(298deg) brightness(108%) contrast(101%)'
    },
    {
        title: 'Rendimiento Deportivo',
        svg: energia,
        pos: 1,
        color: '#b80d57',
        filter: 'invert(14%) sepia(87%) saturate(3705%) hue-rotate(321deg) brightness(92%) contrast(101%)'
    },
    {
        title: 'Perder Grasa',
        svg: calorias,
        pos: 2,
        color: '#e03e36',
        filter: 'invert(46%) sepia(77%) saturate(5681%) hue-rotate(343deg) brightness(92%) contrast(89%)'
    },
 ];

export default ArrayMenu; 