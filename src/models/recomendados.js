const recomendados = [
    {
        nombre: 'Proteina de suero',
        nombre_producto: 'Proteína de suero en polvo Optimum Nutrition',
        link_u: 'https://amzn.to/2YRbXLt',
        link_es: 'https://amzn.to/2YLRdEF',
        asin_u: 'B000QSNYGI',
        asin_es: 'B000GISTZ4',
    },
    {
        nombre: 'Creatina',
        nombre_producto: 'Creatina Monohidratada en polvo',
        link_u: 'https://amzn.to/2WtfwKh',
        link_es: 'https://amzn.to/2QfrlxO',
        asin_u: 'B002DYIZEO',
        asin_es: 'B00T7L20EC',
    },
    {
        nombre: 'Cafeína',
        nombre_producto: 'Pastillas de cafeína de 200 mg',
        link_u: 'https://amzn.to/2YLBJAn',
        link_es: 'https://amzn.to/2HyZDJN',
        asin_u: 'B01DFQSVK6',
        asin_es: 'B00L7WT7I6',
    }
]
export default recomendados